package org.sh.user;

/**
 * Created by sheralam on 5/12/17.
 */
public class Host implements User {


    public Host(String name, int id, int type) {
        this.name = name;
        this.id = id;
        this.type = type;
    }

    @Override
    public String getName() {
        return name;
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getType() {
        return type;
    }


    private final String name;
    private final int id;
    private final int type;



    @Override
    public int hashCode() {
        return  (7 + getId()+getType()) * getName().hashCode();
    }

    @Override
    public String toString() {
        return getName();
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode() == obj.hashCode();
    }
}
