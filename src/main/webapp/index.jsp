<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    long date = new Date().getTime();
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Quiz Magic</title>
    <link rel="stylesheet" href="static/quiz.css?build=<%=date%>">
    <link rel="stylesheet" href="static/index.css?build=<%=date%>">
    <script src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
</head>
<body>


<form action="WebSocketClient.jsp" method="post" id="login_form" class="login">
    <div id="join_quiz">
        <div>
            <span>Enter Quiz :</span> <input type="text" name="quiz_id">
        </div>
        <div>
            <span>Your  Name :</span> <input type="text" name="participant_name">
        </div>

        <button type="submit">Enter</button>
    </div>
</form>


</body>
<script src="static/quiz.js"></script>
<script src="static/index.js"></script>
</html>