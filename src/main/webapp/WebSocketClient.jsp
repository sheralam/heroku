<%@ page import="java.util.Date" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%
    long date = new Date().getTime();
%>
<c:if test="${empty applicationScope.quiz_created}">
    <c:set var="quiz_created" scope="application"></c:set>
</c:if>
<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Quiz Magic</title>

    <script
            src="https://code.jquery.com/jquery-1.12.4.min.js"
            integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
            crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <link rel="stylesheet" href="static/quiz.css?build=<%=date%>">
    <link rel="stylesheet" href="static/ws.css?build=<%=date%>">
</head>
<body class="client">
<form style="display: none;">
    <input id="message" type="text">
    <input onclick="sendMessage();" value="Echo" type="button">
    <input onclick="closeConnection();" value="Disconnect" type="button">

    <br>
    <textarea id="echoText" rows="5" cols="30"></textarea>
</form>

<div id="mainBlock">
    <div id="waitingForQuestion">
        <h1> Waiting For Quiz to Start !!! </h1>
        <div>
            <img src="static/gfx/loading-image.gif" width="240" height="240" style="margin-left: 30%;">
        </div>
    </div>
    <div id="showMultipleAnswer">
        <div><h1>Select The right Answer</h1></div>
        <div>
            <h2 id="question"></h2>
            <div id="answer0" index="0" class="answer">answer0</div>
            <div id="answer1" index="1" class="answer">answer1</div>
            <div id="answer2" index="2" class="answer">answer2</div>
            <div id="answer3" index="3" class="answer">answer3</div>
            <input type="hidden" id="qIndex" value="0" />
            <input type="hidden" id="correctAnswer" value="-1" />
            <input type="hidden" id="answerGiven" value="-1" />

        </div>
    </div>
    <div id="showResult">
        <div><h1>Quiz is finished !!!</h1></div>
        <div>
            <h2 >Total Questions :: <span id="qTotalQuestions"></span></h2>
            <h2 >Correct Answer Given Questions :: <span id="qCorrectAnswered"></span></h2>

        </div>
    </div>


</div>


<script type="text/javascript">

</script>
</body>
<script type="text/javascript">
    var QuizId = ${param.quiz_id};
    var participantName = "${param.participant_name}";

</script>

<script src="static/Utils.js?build=<%=date%>"></script>
<script src="static/receiver.js?build=<%=date%>"></script>

<script src="static/ws.js?build=<%=date%>"></script>
</html>