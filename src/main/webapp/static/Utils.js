/**
 * Created by sheralam on 5/14/17.
 */
var Utils = Utils || {};

(
    function(module){

        function getRandomColor() {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++ ) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        }
        function getRandomSingleInteger() {
            return Math.floor(Math.random() * 10);
        }

        /* return msg objects only, deducts redirection part*/
        function getJsonMsgObject(msg){
            var obj = JSON.parse(msg);
            return obj.msg;
        }



        var ifStorage;
        var memStorage;
        (function validateStorage(){
            ifStorage = typeof(Storage);
            if (ifStorage !== 'undefined') {
                var result = {};
                result.name = "nobody";
                result.correctAnswer = 0;
                sessionStorage.setItem("result", JSON.stringify(result));

                var totalQ = {};
                totalQ.count = 0;
                sessionStorage.setItem("totalQ", JSON.stringify(totalQ));
            }
        })();

        function setResult(name) {

            if (ifStorage !== 'undefined') {
                var result = {};
                result.name = name;
                result.correctAnswer = (+ (getResult("result").correctAnswer))+1;
                sessionStorage.setItem("result", JSON.stringify(result));
            } else {
                memStorage = memStorage || {};
                memStorage.name = name;
                memStorage.correctAnswer = (+ (getResult("result").correctAnswer))+1;
                memStorage.count = (+ (getResult("result").count))+1;

            }
        }

        function updateCount(){
            if (ifStorage !== 'undefined') {
                var totalQ = {};
                totalQ.count = (+ (getResult("totalQ").count))+1;
                sessionStorage.setItem("totalQ", JSON.stringify(totalQ));
            } else {
                memStorage = memStorage || {};
                memStorage.count = (+ (getResult("totalQ").count))+1;

            }
        }

        function getResult(itemName){

            if (ifStorage !== 'undefined') {
               return     JSON.parse(sessionStorage.getItem(itemName));
            }
            else {
                return memStorage;
            }
        }

        function isCorrectAnswer(){
            return (Number($("#showMultipleAnswer #answerGiven").val()) == Number($("#showMultipleAnswer #correctAnswer").val())) ? 1 : 0;
        }

        module.getRandomColor = getRandomColor;
        module.getRandomSingleInteger = getRandomSingleInteger;
        module.getJsonMsgObject = getJsonMsgObject;
        module.setResult = setResult;
        module.getResult = getResult;
        module.isCorrectAnswer = isCorrectAnswer;
        module.updateCount = updateCount;



    }

)(Utils);