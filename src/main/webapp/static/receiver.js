/**
 * Created by sheralam on 5/14/17.
 */
function init(webSocket) {
    /*
     // Connection msg
     {
     "redirectClient":"0",
     "msg":{
         "msgType":"connection",
         "name":"participantName"
         }
     }
     */
    function wsOpen(message) {
        echoText.value += "Connected ... \n";
        wsSendMessage('{"redirectClient":"0","msg":{"msgType":"connection","name":"' + participantName + '"}}');

    }

    function wsSendMessage(message) {
        webSocket.send(message);
        console.log("Message sent to the server : " + message);

    }

    function wsCloseConnection() {
        webSocket.close();
    }


    /*
     // Question received

     */
    function wsGetMessage(message) {

        var msgObj = Utils.getJsonMsgObject(message.data);
        console.log("Message received from to the server : "+msgObj.msgType);
        console.log(msgObj);
        if (msgObj.msgType == "question") {
            setQustions(msgObj.q,msgObj.qIndex);
 //           ping();
        }else if (msgObj.msgType == "showAnswer") {
            var showAnswer = '#answer'+$("#showMultipleAnswer #correctAnswer").val();
            $(showAnswer).addClass('correct');
            Utils.updateCount();
            if(Utils.isCorrectAnswer() == 1){
                storeResult();
            }
        }else if (msgObj.msgType == "sendResult") {
            var showAnswer = '#answer'+$("#showMultipleAnswer #correctAnswer").val();
            $(showAnswer).addClass('correct');
            Utils.updateCount();
            if(Utils.isCorrectAnswer() == 1){
                storeResult();
            }
            sendResult();
        }else if (msgObj.msgType == "restartQuiz") {
            wsCloseConnection();
            console.log("restarting");
            setTimeout(function () {
                window.location.href = "/";
            }, 500);

        }
    }

    function sendResult(){
        showResult();
        setTimeout(function () {
            var res = Utils.getResult("result").correctAnswer;
            wsSendMessage('{"redirectClient":"0","msg":{"msgType":"totalResult","name":"' + participantName + '","totalCorrectAnswer":"' + res + '"}}');
        },200);

    }

    function showResult(){

        $(".client #showMultipleAnswer").hide();

        $(".client #showResult").show();

        var totalQ = Utils.getResult("totalQ");
        var result = Utils.getResult("result");

        $(".client #showResult #qTotalQuestions").text(totalQ.count);
        $(".client #showResult #qCorrectAnswered").text(result.correctAnswer);


    }


    function setQustions(q,qIndex){
            console.log("setting up question");
            console.log(q);
            $(".client #showMultipleAnswer").show();
            $(".client #waitingForQuestion").hide();
            $(".client #showMultipleAnswer div").removeClass("selected correct");
            $(".client #showMultipleAnswer div").removeClass("correct");

            $(".client #showMultipleAnswer #question").text(q.question);
            $(".client #showMultipleAnswer #answer0").text(q.answers[0]);
            $(".client #showMultipleAnswer #answer1").text(q.answers[1]);
            $(".client #showMultipleAnswer #answer2").text(q.answers[2]);
            $(".client #showMultipleAnswer #answer3").text(q.answers[3]);
            $(".client #showMultipleAnswer #qIndex").val(qIndex);
            $(".client #showMultipleAnswer #correctAnswer").val(q.correctAnswer);
            $(".client #showMultipleAnswer #answerGiven").val("-1");
            $(".client #showMultipleAnswer").animate({
                color:"#dc0000"
            }, 3000);
            setTimeout(function(){
                $(".client #showMultipleAnswer").animate({
                    color:"#000"
                }, 3000);
            },3200);
    }

    function ping(){
        setTimeout(function () {
            wsSendMessage('{"redirectClient":"0","msg":{"msgType":"ping","name":"' + participantName + '"}}');
            ping();
        },2000);

    }

    function sendAnswer(){
        setTimeout(function () {
            wsSendMessage('{"redirectClient":"0","msg":{"msgType":"answer","name":"' + participantName + '"}}');
        },200);

    }

    /*
     // Connection msg
     {
     "redirectClient":"0",
     "msg":{
     "msgType":"answer",
     "qIndex":"0"
     "isCorrectAnswer":0/1
     }
     }
     */

    function sendAnswer1(qIndex,isCorrectAnswer){
        var messageObj = {};
        messageObj.redirectClient = 0;
        var msg = {};
        msg.msgType = "answer";
        msg.qIndex = qIndex;
        msg.isCorrectAnswer = isCorrectAnswer;

        messageObj.msg = msg;
        console.log(JSON.stringify(messageObj));
        wsSendMessage(JSON.stringify(messageObj));

    }


    function wsClose(message) {
        echoText.value += "Disconnect ... \n";
    }

    function wsError(message) {
        echoText.value += "Error ... \n";
    }

    function storeResult(){
        Utils.setResult(participantName);
    }

    return {
        wsOpen: wsOpen,
        wsSendMessage: wsSendMessage,
        wsCloseConnection: wsCloseConnection,
        wsGetMessage: wsGetMessage,
        wsClose: wsClose,
        sendAnswer:sendAnswer,
        storeResult:storeResult,
        wsError: wsError
    }


};