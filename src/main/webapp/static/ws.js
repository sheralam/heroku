/**
 * Created by sheralam on 5/13/17.
 */

var wsUrl = "wss://" + document.domain + "/websocketendpoint/" + QuizId;
console.log(wsUrl);
var webSocket = new WebSocket(wsUrl);
var echoText = document.getElementById("echoText");
echoText.value = "";
var message = document.getElementById("message");
var socketModule = init(webSocket);
webSocket.onopen = socketModule.wsOpen;
webSocket.onmessage = socketModule.wsGetMessage;
webSocket.onclose = socketModule.wsClose;
webSocket.onerror = socketModule.wsError;


function sendMessage() {
    webSocket.send(message.value);
    echoText.value += "Message sended to the server : " + message.value + "\n";
    message.value = "";
}


function closeConnection() {
    webSocket.close();
}

$(document).ready(function () {

    $(".host #startButton").click(function () {
        socketModule.hostStart();
    });

    $(".host #nextQButton").click(function () {
        socketModule.showQuestion();
    });


    $("#restartQuiz").click(function () {
        $('.host #mainBlock').text("Restarting... Please wait");
        socketModule.restartQuiz();

        setTimeout(function () {
            webSocket.close();
        }, 2500);
        console.log("restarting");
        setTimeout(function () {
            window.location.href = "/create.jsp";
        }, 3000);
    });


    $("#showMultipleAnswer .answer").click(function () {
        var answerGiven = Number($("#showMultipleAnswer #answerGiven").val());
        console.log("initial answerGiven " + answerGiven);
        if (answerGiven < 0) {
            $(this).addClass('selected');
            var ans = Number($(this).attr("index"));
            $("#showMultipleAnswer #answerGiven").val(ans);

            var qIndex = $("#showMultipleAnswer #qIndex").val();
            console.log("Answer given for " + qIndex + "::" + ans);

            var isCorrectAnswer = Utils.isCorrectAnswer();
            console.log("isCorrectAnswer =" + isCorrectAnswer);

            socketModule.sendAnswer(qIndex, isCorrectAnswer);
        }
    });

});